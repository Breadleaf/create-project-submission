#!/usr/bin/python3

# Written by : Bradley Hutchings
# Start Date : Aug 23 2022
# End Date   : ???

from os.path import exists
from sys import argv
from os import name

#
# Dev Notes
#

# TODO: Add a function to pop and split a string into its sub strings
# Example: [0, "Hello"] -> [0, "H", "e", "l", "l", "o"]

#
# Helper Utilities
#

# @func error - produce an error report
# @param message (string) - string to be printed as an error message
# @print exit_code (int) - int to exit program with
def error(message, exit_code):
    if name == "posix":
        print(f"\033[0;31mError\033[0;0m: {message}")
    else:
        print(f"Error: {message}")
    exit(exit_code)

# @func error_at_location - Produce an error report (used in lexing stage)
# @param line_buffer (string) - line of file
# @param line (int) - line number of file
# @param col (int) - column where error is found
def error_at_location(line_buffer, line, col):
    print(f"line: {line} col: {col}")
    print(line_buffer, end="")
    print("~" * (col - 1) + "^")

# @func iota - keep a counter of things
# @param reset (bool) - reset the counter (default: false)
# @return - 1 + previous number returned from function
# @credit - https://www.youtube.com/c/TsodingDaily
iota_counter = 0
def iota(reset = False):
    global iota_counter
    if reset:
        iota_counter = 0
    tmp = iota_counter
    iota_counter += 1
    return tmp

def str_to_fmt(s):
    return_string = ""
    for c in s:
        if c == "\n":
            return_string += "\\n"
            continue
        if c == "\t":
            return_string += "\\t"
            continue
        if c == "\"":
            return_string += "\\\""
            continue
        return_string += c
    return return_string

program_argv = []
program_argc = 0

#
# Special Errors
#

class NumberLiteralError(Exception):
    pass

class StringEscapeError(Exception):
    pass

class UnknownTokenError(Exception):
    pass

#
# Lexer
#

# @class token_type - contains categories of tokens
class token_type():
    _number = iota(True) # 0
    _string = iota()     # 1
    _identifier = iota() # 2
    _assignment = iota() # 3
    _builtin = iota()    # 4
    _evals = iota()      # 5
    _eof = iota()        # 6
    _import = iota()     # 7
    _total = iota()      # 8

# @class builtin - contains builtin functions
class builtin():
    _print = iota(True)   # 0
    _print_stack = iota() # 1
    _input = iota()       # 2
    _if = iota()          # 3  ** Should be token_type
    _end_if = iota()      # 4  ** Should be token_type
    _while = iota()       # 5  ** Should be token_type
    _end_while = iota()   # 6  ** Should be token_type
    _macro = iota()       # 7  ** Should be token_type
    _end_macro = iota()   # 8  ** Should be token_type
    _str_to_num = iota()  # 9
    _num_to_str = iota()  # 10
    _pop = iota()         # 11
    _exit = iota()        # 12
    _add = iota()         # 13
    _sub = iota()         # 14
    _mult = iota()        # 15
    _div = iota()         # 16
    _mod = iota()         # 17
    _rtn = iota()         # 18  ** Should be token_type
    _r_file = iota()      # 19
    _w_file = iota()      # 20
    _split_str = iota()   # 21
    _argv = iota()        # 22
    _argc = iota()        # 23
    _total = iota()       # 24

# @class evaluate - contains types of evaluations
class evaluate():
    _eq = iota(True) # 0
    _gt = iota()     # 1
    _lt = iota()     # 2
    _total = iota()  # 3

# @class token - container for organization of tokens and values
class token():
    def __init__(self, __token_type__, __value__):
        self.__token_type = __token_type__
        self.__value = __value__

    def get_token_type(self):
        return self.__token_type

    def get_value(self):
        return self.__value

# @func print_token - 'pretty print' provided token
# @param __token (@class token) - token to print
def print_token(__token):
    if 8 != token_type._total:
        error("Unimplemented token in function: print_token", 2)
    if 24 != builtin._total:
        error("Unimplemented builtin in function: print_token", 2)
    if 3 != evaluate._total:
        error("Unimplemented evaluation in function: print_token", 2)

    if __token.get_token_type() == token_type._number:
        print("Number", __token.get_value())
    if __token.get_token_type() == token_type._string:
        print("String", __token.get_value())
    if __token.get_token_type() == token_type._identifier:
        print("Identifier", __token.get_value())
    if __token.get_token_type() == token_type._assignment:
        print("Assignment")
    if __token.get_token_type() == token_type._builtin:
        print("Builtin", end=" ")

        if __token.get_value() == builtin._print:
            print("print")
        if __token.get_value() == builtin._print_stack:
            print("print_stack")
        if __token.get_value() == builtin._input:
            print("input")
        if __token.get_value() == builtin._if:
            print("if")
        if __token.get_value() == builtin._end_if:
            print("end_if")
        if __token.get_value() == builtin._while:
            print("while")
        if __token.get_value() == builtin._end_while:
            print("end_while")
        if __token.get_value() == builtin._macro:
            print("macro")
        if __token.get_value() == builtin._end_macro:
            print("end_macro")
        if __token.get_value() == builtin._str_to_num:
            print("str_to_num")
        if __token.get_value() == builtin._num_to_str:
            print("num_to_str")
        if __token.get_value() == builtin._pop:
            print("pop")
        if __token.get_value() == builtin._exit:
            print("exit")
        if __token.get_value() == builtin._add:
            print("add")
        if __token.get_value() == builtin._sub:
            print("sub")
        if __token.get_value() == builtin._mult:
            print("mult")
        if __token.get_value() == builtin._div:
            print("div")
        if __token.get_value() == builtin._mod:
            print("mod")
        if __token.get_value() == builtin._r_file:
            print("file read")
        if __token.get_value() == builtin._w_file:
            print("file write")
        if __token.get_value() == builtin._split_str:
            print("split string")
        if __token.get_value() == builtin._argv:
            print("argv")
        if __token.get_value() == builtin._argc:
            print("argc")
        if __token.get_value() == builtin._rtn:
            print("rtn")

    if __token.get_token_type() == token_type._evals:
        print("Evaluation", end=" ")

        if __token.get_value() == evaluate._eq:
            print("=")
        if __token.get_value() == evaluate._gt:
            print(">")
        if __token.get_value() == evaluate._lt:
            print("<")

    if __token.get_token_type() == token_type._eof:
        print("EOF")
    if __token.get_token_type() == token_type._import:
        print("import")

# @func list_to_gen - convert a list to a generator object
# @param l (list) - list to convert into a generator object
# @return - generator object from input
def list_to_gen(l):
    for el in l:
        yield  el

# @func lex - read a file char by char and build a list of tokens
# @param char_list (char list) - generator object of chars
# @param line_number (int) - line number of currently evaluated line
# @return - list of @token
def lex(char_list, line_number):
    if 8 != token_type._total:
        error("Unimplemented token in function: lex", 2)
    if 24 != builtin._total:
        error("Unimplemented builtin in function: lex", 2)
    if 3 != evaluate._total:
        error("Unimplemented evaluation in function: lex", 2)

    chars = list_to_gen(char_list)
    current_char_position = 0
    last_char = " "
    tokens = []

    while True:
        try:
            identifier_string = ""

            while last_char.isspace():
                last_char = chars.__next__()
                current_char_position += 1

            # Check for types and identifiers
            if last_char.isalpha() or last_char == "_":
                identifier_string = last_char

                while (last_char := chars.__next__()).isalnum():
                    current_char_position += 1
                    identifier_string += last_char
                current_char_position += 1

                # Builtin functions
                if identifier_string == "print":
                    tokens.append(token(token_type._builtin, builtin._print))
                    continue
                if identifier_string == "printStack":
                    tokens.append(token(token_type._builtin, builtin._print_stack))
                    continue
                if identifier_string == "input":
                    tokens.append(token(token_type._builtin, builtin._input))
                    continue
                if identifier_string == "if":
                    tokens.append(token(token_type._builtin, builtin._if))
                    continue
                if identifier_string == "fi":
                    tokens.append(token(token_type._builtin, builtin._end_if))
                    continue
                if identifier_string == "while":
                    tokens.append(token(token_type._builtin, builtin._while))
                    continue
                if identifier_string == "stop":
                    tokens.append(token(token_type._builtin, builtin._end_while))
                    continue
                if identifier_string == "macro":
                    tokens.append(token(token_type._builtin, builtin._macro))
                    continue
                if identifier_string == "end":
                    tokens.append(token(token_type._builtin, builtin._end_macro))
                    continue
                if identifier_string == "toNumber":
                    tokens.append(token(token_type._builtin, builtin._str_to_num))
                    continue
                if identifier_string == "toString":
                    tokens.append(token(token_type._builtin, builtin._num_to_str))
                    continue
                if identifier_string == "pop":
                    tokens.append(token(token_type._builtin, builtin._pop))
                    continue
                if identifier_string == "exit":
                    tokens.append(token(token_type._builtin, builtin._exit))
                    continue
                if identifier_string == "read":
                    tokens.append(token(token_type._builtin, builtin._r_file))
                    continue
                if identifier_string == "write":
                    tokens.append(token(token_type._builtin, builtin._w_file))
                    continue
                if identifier_string == "splitString":
                    tokens.append(token(token_type._builtin, builtin._split_str))
                    continue
                if identifier_string == "argv":
                    tokens.append(token(token_type._builtin, builtin._argv))
                    continue
                if identifier_string == "argc":
                    tokens.append(token(token_type._builtin, builtin._argc))
                    continue
                if identifier_string == "import":
                    tokens.append(token(token_type._import, None))
                    continue

                # Identifier
                tokens.append(token(token_type._identifier, identifier_string))
                continue

            # Check for "number" literal
            if last_char.isdigit() or last_char == "." or last_char == "-":
                number_string = ""
                in_decimal = False

                if last_char == "-":
                    last_char = chars.__next__()
                    if not last_char.isdigit() and not last_char == ".":
                        tokens.append(token(token_type._builtin, builtin._sub))
                        continue
                    number_string += "-"

                while True:
                    if in_decimal and last_char == ".":
                        raise NumberLiteralError
                    if not in_decimal and last_char == ".":
                        in_decimal = True
                    number_string += last_char
                    last_char = chars.__next__()
                    current_char_position += 1
                    if not last_char.isdigit() and not last_char == ".":
                        break

                tokens.append(token(token_type._number, float(number_string)))
                continue

            # Check for string literal
            if last_char == "\"":
                while (last_char := chars.__next__()) != "\"":
                    if last_char == "\\":
                        last_char = chars.__next__()
                        current_char_position += 1
                        if last_char == "n":
                            identifier_string += "\n"
                            current_char_position += 1
                            continue
                        elif last_char == "t":
                            identifier_string += "\t"
                            current_char_position += 1
                            continue
                        elif last_char == "\"":
                            identifier_string += "\""
                            current_char_position += 1
                            continue
                        else:
                            raise StringEscapeError
                    current_char_position += 1
                    identifier_string += last_char
                last_char = chars.__next__() # Eat exit quote
                current_char_position += 1

                tokens.append(token(token_type._string, identifier_string))
                continue

            # Skip comments
            if last_char == "#":
                while True:
                    last_char = chars.__next__()
                    current_char_position += 1
                    if last_char == "\n":
                        break
                continue

            if last_char == ":":
                last_char = chars.__next__()
                current_char_position += 1
                tokens.append(token(token_type._assignment, None))
                continue

            if last_char == "=":
                last_char = chars.__next__()
                current_char_position += 1
                tokens.append(token(token_type._evals, evaluate._eq))
                continue

            if last_char == ">":
                last_char = chars.__next__()
                current_char_position += 1
                tokens.append(token(token_type._evals, evaluate._gt))
                continue

            if last_char == "<":
                last_char = chars.__next__()
                current_char_position += 1
                tokens.append(token(token_type._evals, evaluate._lt))
                continue

            if last_char == "+":
                last_char = chars.__next__()
                current_char_position += 1
                tokens.append(token(token_type._builtin, builtin._add))
                continue

            if last_char == "-":
                last_char = chars.__next__()
                current_char_position += 1
                tokens.append(token(token_type._builtin, builtin._sub))
                continue

            if last_char == "*":
                last_char = chars.__next__()
                current_char_position += 1
                tokens.append(token(token_type._builtin, builtin._mult))
                continue

            if last_char == "/":
                last_char = chars.__next__()
                current_char_position += 1
                tokens.append(token(token_type._builtin, builtin._div))
                continue

            if last_char == "%":
                last_char = chars.__next__()
                current_char_position += 1
                tokens.append(token(token_type._builtin, builtin._mod))
                continue

            raise UnknownTokenError

        except UnknownTokenError:
            error_at_location("".join(char_list), line_number, current_char_position)
            error("Unknown token", 1)
        except NumberLiteralError:
            error_at_location("".join(char_list), line_number, current_char_position)
            error("Invalid number literal", 1)
        except StringEscapeError:
            error_at_location("".join(char_list), line_number, current_char_position)
            error("Unknown string escape", 1)
        except:
            break

    return tokens

# @func prepare_file - turn file name into a list of @token
# @param file_name (string) - string containing path to file
# @return - list of @token
def prepare_file(file_name):
    buffer = None
    try:
        buffer = [*map(list, [*open(file_name).readlines()])]
    except IsADirectoryError:
        error("Must provide a file not directory", 1)

    token_list = []
    line_number = 1
    for line in buffer:
        for tok in lex(line, line_number):
            token_list.append(tok)
        line_number += 1
    return token_list

#
# Driver (main loop)
#

stack = []
macros = {}
variables = {}
if_nest_count = 0
while_nest_count = 0

# @func main_loop - where all tokens are evaluated and converted to commands
# @param tokens (list of @token) - list of @token to evaluate
# @return - returns None to escape macros, while loops and if statements
def main_loop(tokens):
    if 8 != token_type._total:
        error("Unimplemented token in function: main_loop", 2)
    if 24 != builtin._total:
        error("Unimplemented builtin in function: main_loop", 2)
    if 3 != evaluate._total:
        error("Unimplemented evaluation in function: main_loop", 2)

    global stack
    global macros
    global variables
    global if_nest_count
    global while_nest_count

    while True:
        current_token = tokens.__next__()

        # Imports
        if current_token.get_token_type() == token_type._import:
            include_path = ["", "/usr/lib/leaf-lang/", "./std/"]
            current_token = tokens.__next__()
            if current_token.get_token_type() != token_type._string:
                error("Include expects a string literal", 1)
            inclue_exists = False
            valid_path = None
            for _path in include_path:
                if exists(_path + current_token.get_value()):
                    inclue_exists = True
                    valid_path = _path
                    break
            if not inclue_exists:
                error("Include expects a valid path to a file", 1)
            temp = prepare_file(valid_path + current_token.get_value())
            temp.append(token(token_type._builtin, builtin._rtn))
            main_loop(list_to_gen(temp))
            continue

        # Builtins
        if current_token.get_token_type() == token_type._builtin:
            if current_token.get_value() == builtin._print:
                if len(stack) == 0:
                    error("Cannot print from empty stack", 1)
                print(stack.pop(), end="")
            if current_token.get_value() == builtin._print_stack:
                print(f"\n{stack}\n")
            if current_token.get_value() == builtin._input:
                stack.append(input())
            if current_token.get_value() == builtin._if:
                if stack[-1] == True:
                    temp = []
                    if_nest_count = 1
                    while True:
                        current_token = tokens.__next__()
                        if current_token.get_token_type() == token_type._builtin:
                            if current_token.get_value() == builtin._if:
                                if_nest_count += 1
                            if current_token.get_value() == builtin._end_if:
                                if_nest_count -= 1
                        if current_token.get_token_type() == token_type._eof:
                            error("Expected end of if statement", 1)
                        if if_nest_count == 0:
                            break
                        temp.append(current_token)

                    temp.append(token(token_type._builtin, builtin._rtn))
                    main_loop(list_to_gen(temp))
                else:
                    if_nest_count = 1
                    while True:
                        current_token = tokens.__next__()
                        if current_token.get_token_type() == token_type._builtin:
                            if current_token.get_value() == builtin._if:
                                if_nest_count += 1
                            if current_token.get_value() == builtin._end_if:
                                if_nest_count -= 1
                        if if_nest_count == 0:
                            break
                        if current_token.get_token_type() == token_type._eof:
                            error("Expected end of if statement", 1)
            if current_token.get_value() == builtin._end_if:
                pass
            if current_token.get_value() == builtin._while:
                if stack[-1] == True:
                    temp = []
                    while_nest_count = 1
                    while True:
                        current_token = tokens.__next__()
                        if current_token.get_token_type() == token_type._builtin:
                            if current_token.get_value() == builtin._while:
                                while_nest_count += 1
                            if current_token.get_value() == builtin._end_while:
                                while_nest_count -= 1
                        if current_token.get_token_type() == token_type._eof:
                            error("Expected end of while statement", 1)
                        if while_nest_count == 0:
                            break
                        temp.append(current_token)

                    temp.append(token(token_type._builtin, builtin._rtn))

                    while stack[-1] == True:
                        main_loop(list_to_gen(temp))
                else:
                    while_nest_count = 1
                    while True:
                        current_token = tokens.__next__()
                        if current_token.get_token_type() == token_type._builtin:
                            if current_token.get_value() == builtin._while:
                                while_nest_count += 1
                            if current_token.get_value() == builtin._end_while:
                                while_nest_count -= 1
                        if while_nest_count == 0:
                            break
                        if current_token.get_token_type() == token_type._eof:
                            error("Expected end of while statement", 1)
            if current_token.get_value() == builtin._end_while:
                pass
            if current_token.get_value() == builtin._macro:
                current_token = tokens.__next__()
                if current_token.get_token_type() != token_type._identifier:
                    error("Expected identifier", 1)
                if current_token.get_value() in variables.keys():
                    error("Macro identifier cannot have the same name as a variable", 1)
                tmp_id = current_token.get_value()
                temp = []
                while True:
                    current_token = tokens.__next__()
                    if current_token.get_token_type() == token_type._builtin:
                        if current_token.get_value() == builtin._macro:
                            error("Cannot next macros", 1)
                        if current_token.get_value() == builtin._end_macro:
                            break
                    if current_token.get_token_type() == token_type._eof:
                        error("Expected end of macro statement", 1)
                    temp.append(current_token)
                temp.append(token(token_type._builtin, builtin._rtn))
                macros.update({tmp_id : temp})
            if current_token.get_value() == builtin._end_macro:
                pass
            if current_token.get_value() == builtin._str_to_num:
                try:
                    stack.append(float(stack.pop()))
                except:
                    pass
            if current_token.get_value() == builtin._num_to_str:
                stack.append(str(stack.pop()))
            if current_token.get_value() == builtin._pop:
                stack.pop()
            if current_token.get_value() == builtin._exit:
                if len(stack) == 0:
                    exit(0)
                try:
                    exit(int(stack.pop()))
                except Exception as ex:
                    exit(0)
            if current_token.get_value() == builtin._add:
                a = stack.pop()
                b = stack.pop()
                a_num = False
                b_num = False
                if isinstance(a, float) or isinstance(a, int):
                    a_num = True
                if isinstance(b, float) or isinstance(b, int):
                    b_num = True
                if (a_num and b_num) or (not a_num and not b_num):
                    stack.append(b + a)
                    continue
                else:
                    stack.append(str(b) + str(a))
            if current_token.get_value() in [builtin._sub, builtin._mult, builtin._div, builtin._mod]:
                a = b = None
                try:
                    a = float(stack.pop())
                    b = float(stack.pop())
                except ValueError:
                    error("Top two values of stack must be of type number to use math operator", 1)
                if current_token.get_value() == builtin._sub:
                    stack.append(b - a)
                elif current_token.get_value() == builtin._mult:
                    stack.append(b * a)
                elif current_token.get_value() == builtin._div:
                    stack.append(b / a)
                elif current_token.get_value() == builtin._mod:
                    stack.append(int(b) % int(a))
                else:
                    error("Unknown operator failure in builtin math", 1)
            if current_token.get_value() == builtin._r_file:
                a = str(stack.pop()) # File to read from
                if not exists(a):
                    error(f"File read - invalid file path: {a}", 1)
                stack.append(open(a).read()[:-1])
            if current_token.get_value() == builtin._w_file:
                a = str(stack.pop()) # String to write to file
                b = str(stack.pop()) # File to write to
                open(b, "w").write(a + "\n")
            if current_token.get_value() == builtin._split_str:
                stack += [c for c in stack.pop()]
            if current_token.get_value() == builtin._argv:
                stack += program_argv
            if current_token.get_value() == builtin._argc:
                stack.append(program_argc)
            if current_token.get_value() == builtin._rtn:
                return None

        # Evals
        elif current_token.get_token_type() == token_type._evals:
            if len(stack) < 2:
                error("Cannot evaluate from stack with less than two values", 1)
            a = stack.pop()
            b = stack.pop()
            c = None
            if current_token.get_value() == evaluate._eq:
                c = True if a == b else False
            if current_token.get_value() == evaluate._gt:
                c = True if b > a else False
            if current_token.get_value() == evaluate._lt:
                c = True if b < a else False
            stack.append(b)
            stack.append(a)
            stack.append(c)
        elif current_token.get_token_type() == token_type._identifier:
            if current_token.get_value() in variables.keys():
                stack.append(variables[current_token.get_value()])
            elif current_token.get_value() in macros.keys():
                main_loop(list_to_gen(macros[current_token.get_value()]))
            else:
                error("Referenced non initialized variable or macro", 1)
        elif current_token.get_token_type() == token_type._assignment:
            current_token = tokens.__next__()
            if current_token.get_token_type() != token_type._identifier:
                error("Expected identifier", 1)
            if current_token.get_value() in macros.keys():
                error("Variables cannot share a name with an existing macro", 1)
            variables.update({current_token.get_value() : stack.pop()})
        elif current_token.get_token_type() == token_type._number:
            stack.append(current_token.get_value())
        elif current_token.get_token_type() == token_type._string:
            stack.append(current_token.get_value())
        elif current_token.get_token_type() == token_type._eof:
            exit(0)
        else:
            print_token(current_token)
            error("Unexpected token found", 1)

        if current_token.get_token_type() == token_type._eof:
            exit(0)

#
# Transpiler
#

# TODO: Dont make it always print a tab depending on the block_depth instead
#       Make it print a tab as needed.
block_depth = 0
def transpile_to_python(tokens):
    if 8 != token_type._total:
        error("Unimplemented token in function: transpile_to_python", 2)
    if 24 != builtin._total:
        error("Unimplemented builtin in function: transpile_to_python", 2)
    if 3 != evaluate._total:
        error("Unimplemented evaluation in function: transpile_to_python", 2)

    global macros
    global variables
    global block_depth
    return_string = ""

    while True:
        current_token = tokens.__next__()

        return_string += ("\t" * (if_nest_count + while_nest_count + block_depth))

        # Imports
        if current_token.get_token_type() == token_type._import:
            include_path = ["", "/usr/lib/leaf-lang/"]
            current_token = tokens.__next__()
            if current_token.get_token_type() != token_type._string:
                error("Include expects a string literal", 1)
            inclue_exists = False
            valid_path = None
            for _path in include_path:
                if exists(_path + current_token.get_value()):
                    inclue_exists = True
                    valid_path = _path
                    break
            if not inclue_exists:
                error("Include expects a valid path to a file", 1)
            temp = prepare_file(valid_path + current_token.get_value())
            temp.append(token(token_type._builtin, builtin._rtn))
            return_string += transpile_to_python(list_to_gen(temp))
            continue


        # Builtins
        if current_token.get_token_type() == token_type._builtin:
            if current_token.get_value() == builtin._print:
                return_string += 'print(stack.pop(), end="")\n'
            if current_token.get_value() == builtin._print_stack:
                return_string += 'print(f"\\n{stack}\\n")\n'
            if current_token.get_value() == builtin._input:
                return_string += 'stack.append(input())\n'
            if current_token.get_value() == builtin._if:
                return_string += 'if stack[-1] == True:\n'
                block_depth += 1
            if current_token.get_value() == builtin._while:
                return_string += "while stack[-1] == True:\n"
                block_depth += 1
            if current_token.get_value() in [builtin._end_if, builtin._end_while]:
                return_string += '\n'
                block_depth -= 1
            if current_token.get_value() == builtin._macro:
                current_token = tokens.__next__()
                if current_token.get_token_type() != token_type._identifier:
                    error("Expected identifier", 1)
                if current_token.get_value() in variables.keys():
                    error("Macro identifier cannot have the same name as a variable", 1)
                tmp_id = current_token.get_value()
                temp = []
                while True:
                    current_token = tokens.__next__()
                    if current_token.get_token_type() == token_type._builtin:
                        if current_token.get_value() == builtin._macro:
                            error("Cannot nest macros", 1)
                        if current_token.get_value() == builtin._end_macro:
                            break
                    if current_token.get_token_type() == token_type._eof:
                        error("Expected end of macro statement", 1)
                    temp.append(current_token)
                temp.append(token(token_type._builtin, builtin._rtn))
                macros.update({tmp_id : temp})
            if current_token.get_value() == builtin._end_macro:
                pass
            if current_token.get_value() == builtin._str_to_num:
                return_string += "try:\n\tstack.append(float(stack.pop()))\nexcept:\n\tpass\n"
            if current_token.get_value() == builtin._num_to_str:
                return_string += "stack.append(str(stack.pop()))\n"
            if current_token.get_value() == builtin._pop:
                return_string += "stack.pop()\n"
            if current_token.get_value() == builtin._exit:
                return_string += "if len(stack) == 0:\n\t"
                return_string += ("\t" * (block_depth)) + "exit(0)\n"
                return_string += ("\t" * (block_depth)) + "try:\n\t"
                return_string += ("\t" * (block_depth)) + "exit(int(stack.pop()))\n"
                return_string += ("\t" * (block_depth)) + "except Exception as ex:\n\t"
                return_string += ("\t" * (block_depth)) + "exit(0)\n"
            if current_token.get_value() == builtin._add:
                return_string += 'a = stack.pop()\n'
                return_string += ("\t" * (block_depth)) + 'b = stack.pop()\n'
                return_string += ("\t" * (block_depth)) + 'a_num = False\n'
                return_string += ("\t" * (block_depth)) + 'b_num = False\n'
                return_string += ("\t" * (block_depth)) + 'if isinstance(a, float) or isinstance(a, int):\n\t'
                return_string += ("\t" * (block_depth)) + 'a_num = True\n'
                return_string += ("\t" * (block_depth)) + 'if isinstance(b, float) or isinstance(b, int):\n\t'
                return_string += ("\t" * (block_depth)) + 'b_num = True\n'
                return_string += ("\t" * (block_depth)) + 'if (a_num and b_num) or (not a_num and not b_num):\n\t'
                return_string += ("\t" * (block_depth)) + 'stack.append(b + a)\n'
                return_string += ("\t" * (block_depth)) + 'else:\n\t'
                return_string += ("\t" * (block_depth)) + 'stack.append(str(b) + str(a))\n'
            if current_token.get_value() in [builtin._sub, builtin._mult, builtin._div, builtin._mod]:
                return_string += 'a = b = None\n'
                return_string += ("\t" * (block_depth)) + 'try:\n\t'
                return_string += ("\t" * (block_depth)) + 'a = float(stack.pop())\n\t'
                return_string += ("\t" * (block_depth)) + 'b = float(stack.pop())\n'
                return_string += ("\t" * (block_depth)) + 'except ValueError:\n\t'
                return_string += ("\t" * (block_depth)) + 'print("Error: Top two values of stack must be of type number to use math operator")\n\t'
                return_string += ("\t" * (block_depth)) + 'exit(1)\n'
                if current_token.get_value() == builtin._sub:
                    return_string += ("\t" * (block_depth)) + 'stack.append(b - a)\n'
                elif current_token.get_value() == builtin._mult:
                    return_string += ("\t" * (block_depth)) + 'stack.append(b * a)\n'
                elif current_token.get_value() == builtin._div:
                    return_string += ("\t" * (block_depth)) + 'stack.append(b / a)\n'
                elif current_token.get_value() == builtin._mod:
                    return_string += ("\t" * (block_depth)) + 'stack.append(int(b) % int(a))\n'
                else:
                    error("Unknown operator failure in builtin math")
            if current_token.get_value() == builtin._r_file:
                return_string += "stack.append(open(str(stack.pop())).read()[:-1])\n"
            if current_token.get_value() == builtin._w_file:
                return_string += "a = str(stack.pop())\n"
                return_string += ("\t" * (block_depth)) + "b = str(stack.pop())\n"
                return_string += ("\t" * (block_depth)) + "open(b, 'w').write(a)\n"
            if current_token.get_value() == builtin._split_str:
                return_string += "stack += [c for c in stack.pop()]\n"
            if current_token.get_value() == builtin._argv:
                return_string += "stack += sys.argv[1:]\n"
            if current_token.get_value() == builtin._argc:
                return_string += "stack.append(len(sys.argv[1:]))\n"
            if current_token.get_value() == builtin._rtn:
                return return_string

        # Evals
        elif current_token.get_token_type() == token_type._evals:
            return_string += 'a = stack.pop()\n'
            return_string += ("\t" * (block_depth)) + 'b = stack.pop()\n'
            if current_token.get_value() == evaluate._eq:
                return_string += ("\t" * (block_depth)) + 'c = True if a == b else False\n'
            if current_token.get_value() == evaluate._gt:
                return_string += ("\t" * (block_depth)) + 'c = True if b > a else False\n'
            if current_token.get_value() == evaluate._lt:
                return_string += ("\t" * (block_depth)) + 'c = True if b < a else False\n'
            return_string += ("\t" * (block_depth)) + 'stack.append(b)\n'
            return_string += ("\t" * (block_depth)) + 'stack.append(a)\n'
            return_string += ("\t" * (block_depth)) + 'stack.append(c)\n'
        elif current_token.get_token_type() == token_type._identifier:
            if current_token.get_value() in variables.keys():
                return_string += 'stack.append(variables["' + current_token.get_value() + '"])\n'
            elif current_token.get_value() in macros.keys():
                return_string += "# Called Macro\n" + transpile_to_python(list_to_gen(macros[current_token.get_value()])) + "\n"
            else:
                error("Referenced non initialized variable or macro", 1)
        elif current_token.get_token_type() == token_type._assignment:
            current_token = tokens.__next__()
            if current_token.get_token_type() != token_type._identifier:
                error("Expected identifier", 1)
            if current_token.get_value() in macros.keys():
                error("Variables cannot share a name with an existing macro", 1)
            variables.update({current_token.get_value() : None})
            return_string += 'variables.update({"' + current_token.get_value() + '" : stack.pop()})\n'
        elif current_token.get_token_type() == token_type._number:
            return_string += 'stack.append(' + str(current_token.get_value()) + ')\n'
        elif current_token.get_token_type() == token_type._string:
            return_string += 'stack.append("' + str_to_fmt(current_token.get_value()) + '")\n'
        elif current_token.get_token_type() == token_type._eof:
            return return_string
        else:
            print_token(current_token)
            error("Unexpected token found", 1)

#
# Program Start
#

def main(file_name, transpile=False):
    token_list = prepare_file(file_name)
    token_list.append(token(token_type._eof, None))

    if transpile:
        file = open("transpiled.py", "w")
        file.write("import sys\n")
        file.write("stack = []\n")
        file.write("variables = {}\n")
        file.write("\n".join([line for line in 
         transpile_to_python(list_to_gen(token_list)).splitlines()
         if not line.isspace()]))


    if not transpile:
        main_loop(list_to_gen(token_list))

if __name__ == "__main__":
    # Ensure that a file is provided
    if len(argv) == 1:
        error("Invalid arg count: 0", 1)

    # Check if transpile mode is on
    transpile_mode = False
    if argv[1] == "-c":
        transpile_mode = True
        del argv[1]

        # If no file is given after transpile flag produce error
        if len(argv) == 1:
            error("Expected file name after '-c' flag", 1)

    # Make sure provided file exists
    if not exists(argv[1]):
        error(f"File not found: {argv[1]}", 1)

    # Set program's arguments
    program_argv = argv[2:]
    program_argc = len(argv[2:])

    main(argv[1], transpile_mode)
