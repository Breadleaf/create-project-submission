# If you aren't too keen on a cutscene, here is the link to the project :)
# https://gitlab.com/Breadleaf/create-project-submission

import time

dialogue = [
        "Hello grader(s)!",
        "I emailed my instructor (Grace) and she said I needed to submit my gitlab link as a python file.",
        "I decided to take that as literally as I could and here we are.",
        "I hope you have a fun time with my project, it is the cumulative work of several months.",
        "Anyways, here you go :)",
        "Project Page: https://gitlab.com/Breadleaf/create-project-submission"
        ]

[(print(x) or time.sleep(2)) for x in dialogue]
