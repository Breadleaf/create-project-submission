# Leaf Lang (csci101 submission version)

This page is best viewed from the [gitlab repository](https://gitlab.com/Breadleaf/create-project-submission).

Hello, this is leaf-lang. A [stack based](https://en.wikipedia.org/wiki/Stack_(abstract_data_type))
programming language. It has a few key features including but not limited to
macros / procedures and (global only) variables. For sake of demonstration, I have included some core
files for the interpreter to work as well as a preconfigured editor. Any and all further
documentation of this language can be found at my
[official gitlab repository](https://gitlab.com/Breadleaf/leaf-lang) for the language.
To give credit where credit is due, every reference used in this project can be found
[here](https://gitlab.com/Breadleaf/leaf-lang#credits).

As for the rest of this repository (and by extension my submission for this project)
There are a few example files that can be ran that are written in my language.
The editor works, however is a little buggy due to some hacky workarounds I implemented
to obtain some extra functionality. It should be noted that all files in this repository
can (and in my honest opinion should) be viewed from a editor that is **not** the
editor I wrote for this project. **All files needed for the editor can be found
in** `./editor` **and likewise the interpreter files can be found in** `./interpreter`.

---

# Get Started

```
git clone https://gitlab.com/Breadleaf/create-project-submission && cd create-project-submission
```

---

# Interpreter

For the interpreter part of this project, there are examples located within the same folder as the interpreter.
Any given example file can be ran with with the interpreter in the following way:
```
python3.10 interpreter.py <file name> <arg 1> <arg 2> ... <arg n>
```
If a file does not require the use of command line arguments, they can be left
ignored. The following is a list of files that need cli input and how to run them:
```
hello.lf -> python3.10 interpreter.py hello.lf <your name>
truth_machine.lf -> python3.10 interpreter.py truth_machine.lf <1 or 0>
miniInterpreter.lf -> python3.10 interpreter.py miniInterpreter.lf miniLang.txt
```
If you do not know what a truth machine is, it is a programming "challenge"
where if a 1 is given it will loop forever, else print 0. The file `miniInterpreter.lf`
is a mini scale concept of me reimplementing the language in it's self. This process
is called [self hosting](https://en.wikipedia.org/wiki/Self-hosting_(compilers)) a language.
Feel free to mess around with the input file to the program `miniLang.txt`, for now
it only supports push operations of numbers, addition and printing the top stack
value.

The interpreter also has another feature, it can source-to-source compile (transpile)
any leaf-lang source code file into an equivalent python source code file. To achieve this
add the `-c` flag as seen below. Note, this will "compile" the program, it must be
run with the python interpreter, also depicted below.
```
python3.10 interpreter.py -c <file name>
python3.10 transpiled.py <arg 1> <arg 2> ... <arg n>
```

---

# Editor

For the editor part of my submission, I have no example files
as it is meant to be more of a sandbox environment if desired. Documentation can be found
[here](https://gitlab.com/Breadleaf/leaf-lang#leaf-lang-ide). The editor
can be started with the command (look to terminal for all program output):
```
python3.10 editor.py
```
A fun side note, you can change the color scheme of the editor by editing the hex color codes
found in the file config.json in the same directory as the editor.

---

# Closing thoughts

The interpreter provided is slightly modified from its official release counterpart.
I had to allow it to search for header files within a set path from the interpreter
so that the header files can import other headers without error.
